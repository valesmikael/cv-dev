import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
	selector: 'app-infosplus',
	templateUrl: './infosplus.component.html',
	styleUrls: ['./infosplus.component.scss']
})
export class InfosplusComponent implements OnInit {

	mouse_is_over = false;
	private $pictures;
	private $puces;

	constructor() {


	}

	ngOnInit() {

		//gestion du slice
		this.$pictures = document.getElementsByClassName('picture');		
		this.$puces = document.getElementsByClassName('puce');		

		setInterval(() => {

			if (!this.mouse_is_over) {
				this.next();
			}

		}, 2500);
		
	}

	puce(event, image) {


		var $active = document.getElementsByClassName("puce_active")[0];	
		console.log(event.target);			
		$active.classList.remove("puce_active");	
		event.target.classList.add('puce_active');

		var $active = document.getElementsByClassName("active")[0];				
		$active.classList.remove("active");								
		document.getElementById(image).classList.add("active");

	}


	// gestion du button prev
	prev() {

		let $active = document.getElementsByClassName("active")[0];
		let $prev = $active.previousElementSibling;

		if (!$prev) $prev = this.$pictures[this.$pictures.length - 1];

		$prev.classList.add("active");
		$active.classList.remove("active");

	}

	// gestion du button next
	next() {

		let $active = document.getElementsByClassName("active")[0];
		let $next = $active.nextElementSibling;

		let $puce_active = document.getElementsByClassName("puce_active")[0];
		let $puce_next = $puce_active.nextElementSibling;

		if (!$next || !$next.classList.contains('picture')) $next = this.$pictures[0];
		if (!$puce_next) $puce_next = this.$puces[0];

		$next.classList.add("active");
		$active.classList.remove("active");

		$puce_next.classList.add("puce_active");
		$puce_active.classList.remove("puce_active");



	}

	// Gestion automatiques

	mouseGetInCarrousel(id: string) {

		this.mouse_is_over = true;
	}

	mouseGetOUtCarrousel(id: string) {

		this.mouse_is_over = false;
	}


















}

