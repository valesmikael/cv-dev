import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
  }

  scroll( id: string ) {

    //on écoute le click sur l'element
    let element: HTMLElement = document.getElementById( id );    

    //on récupère la position par rapport au haut de l'element voulu
    let finalPos = element.offsetTop - 80;

    //on récupère notre position actuel
    let startPos = window.pageYOffset;

    if(finalPos > startPos) {
      // descente 
      //calcul la distance a scroller
      let interval = setInterval(() => {
  
        // on dit que notre position actuelle est 0
        //let currentPos = window.scrollY - startPos;
  
        // on scroll jusqu'à la position voulue
        let targetPos = window.pageYOffset + 120;

        if( targetPos > finalPos ) targetPos = finalPos;
        window.scrollTo(0, targetPos );
  
        // on stop le scroll arrivé à la bonne position
      if( targetPos >= finalPos ) {
          
          clearInterval( interval );
                    
        }     
  
      }, 10);

    }

    if(finalPos < startPos){
    //remontée
    //   //calcul la distance a scroller
    let interval = setInterval(() => {
  
        // on dit que notre position actuelle est 0
        // let currentPos = window.scrollY - startPos;
  
      // on scroll jusqu'à la position voulue
      let targetPos = window.pageYOffset - 100;
      if( targetPos < finalPos ) targetPos = finalPos;

      window.scrollTo(0, targetPos );

        // on stop le scroll arrivé à la bonne position
      if( targetPos <= finalPos ) {          
          clearInterval( interval );
        }     
  
      }, 15);
      

    }
  
  }  

}
